package com.service.loadprototohbase;

import com.config.setVariables;
import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class JoinBuildingEmployeeMapper extends TableMapper<LongWritable, ImmutableBytesWritable> {
    private static final byte[] BUILDINGTABLE = Bytes.toBytes("building");
    private static final byte[] EMPLOYEETABLE = Bytes.toBytes("employee");

    public static final byte[] EMPCF = "employee_details".getBytes();
    public static final byte[] EMPATTR1 = "employee_qual".getBytes();
    private String buildingUri = "hdfs://localhost:8020/ProtoFiles/building.seq";

    private HashMap<String, String> buildingHashMap;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        buildingHashMap =
                GetBuildingCodeHashMap.getBuildingCodeAndCafeteriaCode(
                        buildingUri);
        System.out.println("buildingHashMap : " + buildingHashMap);
    }

    @Override
    public void map(ImmutableBytesWritable rowKey, Result columns, Context context) {
        TableSplit currentSplit = (TableSplit) context.getInputSplit();
        byte[] tableName = currentSplit.getTableName();
        try {
            if (Arrays.equals(tableName, BUILDINGTABLE)) {
                //do nothing
            } else if (Arrays.equals(tableName, EMPLOYEETABLE)) {
                ImmutableBytesWritable value = new ImmutableBytesWritable();
                value.set(columns.getValue(EMPCF, EMPATTR1));
                EmployeeOuterClass.Employee.Builder employee =
                        EmployeeOuterClass.Employee.newBuilder().mergeFrom(value.get());

                String building_code = String.valueOf(employee.getBuildingCode());
                String row = new String(rowKey.get());

                if (buildingHashMap.containsKey(building_code)) {
                    employee.setCafeteriaCode(buildingHashMap.get(building_code));
                    System.out.println("print employee = " + rowKey + " : " + employee);
//                    System.out.println("print = "+rowKey +" : " + new ImmutableBytesWritable
//                            (employee.build().toByteArray()));

                    context.write(new LongWritable(Long.parseLong(row)),
                            new ImmutableBytesWritable(employee.build().toByteArray()));
                } else {
                    context.write(new LongWritable(Long.parseLong(row)),
                            new ImmutableBytesWritable(employee.build().toByteArray()));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
