package com.service.employeeleastattendance;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AttendanceDriver extends Configured implements Tool {
    private String attendanceJobName;
    private String attendanceTableName;
    private String attendancePath;

    public AttendanceDriver(String attendanceJobName, String attendanceTableName,
                            String attendancePath) {
        this.attendanceJobName = attendanceJobName;
        this.attendanceTableName = attendanceTableName;
        this.attendancePath = attendancePath;
    }

    public int run(String[] arg0) {
        List<Scan> scans = new ArrayList<>();

        Scan scan1 = new Scan();
        scan1.setAttribute("scan.attributes.table.name", Bytes.toBytes(attendanceTableName));
        System.out.println(scan1.getAttribute("scan.attributes.table.name"));
        scans.add(scan1);
        Job job = null;
        HBaseConfiguration conf = new HBaseConfiguration();
        try {
            job = new Job(conf, attendanceJobName);

            job.setJarByClass(AttendanceDriver.class);
            FileOutputFormat.setOutputPath(job, new Path(attendancePath));
            job.setOutputKeyClass(IntWritable.class);
            job.setOutputValueClass(IntWritable.class);
            job.setJarByClass(AttendanceDriver.class);
            TableMapReduceUtil.initTableMapperJob(
                    scans,
                    AttendanceMapper.class,
                    IntWritable.class,
                    ImmutableBytesWritable.class,
                    job);
            job.setReducerClass(AttendanceReducer.class);

            job.waitForCompletion(true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
