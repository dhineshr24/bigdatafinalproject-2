package com.service.bulkupload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;

public class BulkLoadDriver extends Configured implements Tool {
    private String bulkLoadInPathFiles;
    private String bulkLoadJobName;
    private String bulkLoadOuthPath;
    private String tableName;

    public BulkLoadDriver(String bulkLoadInPathFiles, String bulkLoadJobName,
                          String bulkLoadOuthPath, String tableName) {
        this.bulkLoadInPathFiles = bulkLoadInPathFiles;
        this.bulkLoadJobName = bulkLoadJobName;
        this.bulkLoadOuthPath = bulkLoadOuthPath;
        this.tableName = tableName;
    }


    private Job createSubmittableJob(Configuration conf, String tableNameString, Path tmpPath, String input) {

        System.out.println("conf: " + conf);
        Job job = null;
        try {
            job = Job.getInstance(conf, bulkLoadJobName);
            job.setJarByClass(BulkLoadMapper.class); //check
            job.setMapperClass(BulkLoadMapper.class);
            job.setMapOutputKeyClass(ImmutableBytesWritable.class);
            job.setMapOutputValueClass(Put.class);
            FileOutputFormat.setOutputPath(job, tmpPath);
            FileInputFormat.addInputPath(job, new Path(input));
            try (Connection connection = ConnectionFactory.createConnection(conf)) {
                TableName tableName = TableName.valueOf(tableNameString);
                Table table = connection.getTable(tableName);
                RegionLocator regionLocator = connection.getRegionLocator(tableName);
                HFileOutputFormat2.configureIncrementalLoad(job, table, regionLocator);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return job;
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = HBaseConfiguration.create(getConf());
        setConf(conf);

        String tableNameString = tableName;
        Path tmpPath = new Path(bulkLoadOuthPath);
        String input = bulkLoadInPathFiles;
        Job job = createSubmittableJob(conf, tableNameString, tmpPath, input);
        boolean success = job.waitForCompletion(true);
        doBulkLoad(tableNameString, tmpPath);
        return success ? 0 : 1;
    }

    private void doBulkLoad(String tableNameString, Path tmpPath) {
        LoadIncrementalHFiles loader = new LoadIncrementalHFiles(getConf());
        try (Connection connection = ConnectionFactory.createConnection(getConf()); Admin admin = connection.getAdmin()) {
            TableName tableName = TableName.valueOf(tableNameString);
            Table table = connection.getTable(tableName);
            RegionLocator regionLocator = connection.getRegionLocator(tableName);
            loader.doBulkLoad(tmpPath, admin, table, regionLocator);
        } catch (TableNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
