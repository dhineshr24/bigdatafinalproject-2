package com.util;

@FunctionalInterface
public interface ICreateCsv {
    void createCsv(int noOfFilesToGenerate, String path, int noOfRowsToGenerate);
}
